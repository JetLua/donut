import {monitor} from './core'
import {game, prepare} from './scenes'

prepare().then(() => {
    game.show()
}).catch(err => {
    wx.showToast({
        title: err.message || err.errMsg || '呀，出错了',
        icon: 'none',
        duration: 5e3
    })
})



