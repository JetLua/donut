import core from '../core'

export default {
    show() {

        this.container = new PIXI.Container()

        this.hand = PIXI.Sprite.from('hand.png')
        this.bkg = PIXI.Sprite.from('logo.donuts.png')
        this.bkg.mask = this.hand
        this.bkg.addChild(this.hand)
        this.hand.position.set(100, 0)
        this.container.addChild(this.bkg)

        core.stage.addChild(this.container)
    },

    hide() {

    }
}